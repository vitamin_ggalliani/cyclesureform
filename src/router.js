import React from "react";
import { Router, Route, IndexRoute } from "react-router";

import App from "./components/App";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import UserForm from "./components/UserForm"
import AddressForm from "./components/AddressForm"
import InfoForm from "./components/InfoForm"
import CycleInfoForm from "./components/CycleInfoForm"
import MandatoryChoices from "./components/MandatoryChoices"
import OptionalExtras from "./components/OptionalExtras"

import { history } from "./store.js";


// build the router
const router = (
  <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={MandatoryChoices} />
      <Route path="options" component={OptionalExtras} />
      <Route path="name" component={UserForm} />
      <Route path="address" component={AddressForm} />
      <Route path="etc" component={InfoForm} />
      <Route path="*" component={NotFound}/>
    </Route>
  </Router>
);

// export
export { router };

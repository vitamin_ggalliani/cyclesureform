import RadioButton from "material-ui/RadioButton"
import RaisedButton from "material-ui/RaisedButton"

import React, { Component } from "react"
import { browserHistory } from "react-router"

import { Field, reduxForm } from "redux-form"
import { RadioButtonGroup, Checkbox } from "redux-form-material-ui"

import validate from "./validate"
import renderField from "./renderField"


class MandatoryChoices extends Component {
render() {
  const { handleSubmit } = this.props
  return(
  <form onSubmit={handleSubmit}>
    <div>
    <label>Accessories</label>
    <Field name="accessories" component={RadioButtonGroup}>
        <RadioButton label="£100" value="100"/>
        <RadioButton label="£200" value="250"/>  
        <RadioButton label="£500" value="500"/>
    </Field>
    </div>
    <div>
    <label>Helmet & Clothing</label>
    <Field name="helmet" component={RadioButtonGroup}>
        <RadioButton label="£100" value="100"/>
        <RadioButton label="£250" value="250"/>
        <RadioButton label="£500" value="500"/>
    </Field>
    <div>
    <label>Replacement Cycle Hire</label>
    <Field name="replacement" component={RadioButtonGroup}>
       <RadioButton label="£100" type="radio" value="100"/>
       <RadioButton label="£250" type="radio" value="250"/>
       <RadioButton label="£500" type="radio" value="500"/>
    </Field>
    </div>
    <label>Legal Cover</label>
    <Field name="legal" component={RadioButtonGroup}>
        <RadioButton name="legal" type="radio" label="£1m" value="1000000"/>
      </Field>  
    </div>
    <RaisedButton type="submit" label="Continue" />
    </form>
  )
}
}

MandatoryChoices = reduxForm({
    form: 'contact',
    destroyOnUnmount: false,
    onSubmit: () => {
      let link = "options"
      browserHistory.push(link)
    },
    validate
})(MandatoryChoices)


export default MandatoryChoices
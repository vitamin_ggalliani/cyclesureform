import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"

import React from "react"
import injectTapEventPlugin from 'react-tap-event-plugin';

import "../stylesheets/main.scss"
import UserForm from "./UserForm"
import AddressForm from "./AddressForm"
import InfoForm from "./InfoForm"
import { store } from "../store"
import MandatoryChoices from "./MandatoryChoices"
import OptionalExtras from "./OptionalExtras"

injectTapEventPlugin();


// app component
export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.state = { 
      page: 1
    }
  }
  nextPage() {
    this.setState({
      page: this.state.page + 1
    })
  }
  previousPage() {
    this.setState({
      page: this.state.page - 1
    })
  }
  sendData() {
    let storeObject = store.getState()
    console.log(storeObject)
  }
  // render
  render() {
    let children = React.cloneElement(this.props.children, {nextPage: this.nextPage})
    const { onSubmit } = this.props
    const { page } = this.state
    return (
    <MuiThemeProvider>
    <div className="container">
        {children}
    </div>
    </MuiThemeProvider>
    );
  }
}

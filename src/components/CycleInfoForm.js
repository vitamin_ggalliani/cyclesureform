import React, { Component } from "react"

import { reduxForm } from "redux-form"
import { browserHistory } from "react-router"

class CycleInfoForm extends Component {
  render() {
    const { handleSubmit } = this.props
    return(
      <form onSubmit = { handleSubmit }>
        <MandatoryChoices/>
        <OptionalExtras />
        <button type="submit">Continue </button> 
      </form>
    )
  }
}

CycleInfoForm = reduxForm({
    form: 'contact',
    destroyOnUnmount: false,
    onSubmit: () => {
      let link = "name"
      browserHistory.push(link)
    }
})(CycleInfoForm)


export default CycleInfoForm
const validate = values => {
  const errors = {}
  if (!values.firstName){
    errors.firstName = "Required"
  }
  if (!values.lastName){
    errors.lastName = "Required"
  }
  if (!values.email) {
    errors.email = "Required"
  } else if (/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b./i.test(values.email)) {
    errors.email = "Invalid email address"
  }
  if (!values.address1) {
    errors.address1 = "Required"
  } 
  if (!values.address2) {
    errors.address2 = "Required"
  }
  if (!values.postcode) {
    errors.postcode = "Required"
  }
  return errors
}

export default validate
import RaisedButton from "material-ui/RaisedButton"

import React, { Component } from "react"
import { browserHistory } from "react-router"

import { Field, reduxForm } from "redux-form"
import { TextField } from "redux-form-material-ui"

import validate from './validate'



class UserForm extends Component {
  render() {
    const { handleSubmit } = this.props
     return (
        <form onSubmit = { handleSubmit } >
          <div>
            <Field hintText="First Name" name = "firstName" component={TextField} />
          </div>
          <div>
            <Field hintText="Last Name" name="lastName" component={TextField} />
          </div>
          <div>
            <Field hintText="Email Address" name="email" component={TextField} type="email" />
          </div>
          <RaisedButton type="submit" label="Continue" />
        </form>
     )
  }
}

UserForm = reduxForm({
    form: 'contact',
    destroyOnUnmount: false, 
    onSubmit: () => {
      let link = "address"
      browserHistory.push(link)
    },
    validate

})(UserForm)

export default UserForm
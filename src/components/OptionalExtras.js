import RaisedButton from "material-ui/RaisedButton"
import RadioButton from "material-ui/RadioButton"

import React, { Component } from "react"

import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import { connect } from 'react-redux'

import { Field, reduxForm, formValueSelector } from "redux-form"

import { RadioButtonGroup, Checkbox, TextField } from "redux-form-material-ui"

import { store } from "../store"



class OptionalExtras extends Component { 
  render() {
  const { handleSubmit } = this.props
  

return(
  <form onSubmit={handleSubmit}>
    <div>
      <Field name="publicLiabilitySelect" label="Public Liability" component={Checkbox}  value={false}/>
        <ReactCSSTransitionGroup 
        transitionName="show"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={0}
        >
          {this.props.publicLiabilitySelect &&
          <div className="option-dropdown" key={0}>
            <Field name="publicLiability" component={RadioButtonGroup}>
              <RadioButton label="£1m" type="radio" value="1000000000"/>
              <RadioButton label="$2m" type="radio" value="2000000000"/>
            </Field>
          </div>
          }
        </ReactCSSTransitionGroup>
    </div>
    <div>
     
      <Field name="personalAccidentSelect" label="Personal Accident" component={Checkbox} value={false}/>
        <ReactCSSTransitionGroup 
        transitionName="show"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={0}
        >
          {this.props.personalAccidentSelect &&
          <div key={0}>
            <Field name="personalAccident" component={RadioButtonGroup}>
              <RadioButton label="£10k" type="radio" value="10000"/>
              <RadioButton label="£15k" type="radio" value="25000"/>
              <RadioButton label="£30k" type="radio" value="30000"/>
              <RadioButton label="£50k" type="radio" value="50000"/>
            </Field>
          </div>
        }
       </ReactCSSTransitionGroup>
    </div>
    <div>
      <Field name="getYouHomeCostsSelect" label="'Get You Home' Costs" component={Checkbox} value={false}/>
      <ReactCSSTransitionGroup 
        transitionName="show"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={0}
        >
        {this.props.getYouHomeCostsSelect &&
        <div key={0}>
          <Field name="getYouHomeCosts" component={RadioButtonGroup}>
            <RadioButton label="£100" type="radio" value="100"/>
          </Field>
        </div>
        }
      </ReactCSSTransitionGroup>
    </div>
    <div>
      
      <Field name="competitionUseSelect" label="Competition Use" component={Checkbox} value={false} />
      <ReactCSSTransitionGroup 
        transitionName="show"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={0}
        >
        {this.props.competitionUseSelect &&
        <div key={0}>
          <Field name="competitionUse" component={RadioButtonGroup}>
            <RadioButton label="YES" value="yes"/>
            <RadioButton label="NO " value="no"/> 
          </Field> 
        </div>
        }
      </ReactCSSTransitionGroup>
    </div>
    <div>
      <Field name="overseasExtensionSelect" label="Overseas Extension" component={Checkbox} value={false}/>
      <ReactCSSTransitionGroup 
        transitionName="show"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={0}
        >
        {this.props.overseasExtensionSelect &&
        <div key={0}>
        <Field name="overseasExtension" component={RadioButtonGroup}>
          <RadioButton label="UK" value="uk"/>
          <RadioButton label="EU" value="eu"/>
        </Field>
        </div>
        }
      </ReactCSSTransitionGroup>
    </div>
    <div>
    
    <Field label="Family Use" name="numberOfRidersSelect" type="checkbox" component={Checkbox} value={false}/>
    <ReactCSSTransitionGroup 
        transitionName="show"
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionEnterTimeout={0}
        transitionLeaveTimeout={0}
        >
        {this.props.numberOfRidersSelect &&
          <div key={0}>
              <Field hintText="Number of Riders" name="numberOfRiders" component={TextField} />
          </div>
        }
    </ReactCSSTransitionGroup>
    </div>
    <div>
    <RaisedButton type="submit" label="Continue" />
    </div>
    </form>
    )
  }
}

OptionalExtras = reduxForm({
    form: 'contact',
    destroyOnUnmount: false,
})(OptionalExtras)

const selector = formValueSelector("contact")
OptionalExtras = connect(
  state => {
   const {  publicLiabilitySelect, 
            personalAccidentSelect, 
            getYouHomeCostsSelect, 
            competitionUseSelect, 
            overseasExtensionSelect, 
            numberOfRidersSelect } =  selector(state, "publicLiabilitySelect", 
                                                      "personalAccidentSelect", 
                                                      "getYouHomeCostsSelect", 
                                                      "competitionUseSelect", 
                                                      "overseasExtensionSelect", 
                                                      "numberOfRidersSelect")
    return {
      publicLiabilitySelect,
      personalAccidentSelect,
      getYouHomeCostsSelect,
      competitionUseSelect,
      overseasExtensionSelect,
      numberOfRidersSelect
    }
  }
)(OptionalExtras)


export default OptionalExtras
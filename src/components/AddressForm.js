import RaisedButton from "material-ui/RaisedButton"

import React, { Component } from "react"
import { browserHistory } from "react-router"


import { Field, reduxForm } from "redux-form"
import { TextField } from "redux-form-material-ui"

import validate from './validate'


class AddressForm extends Component {
  render() {
    const { handleSubmit } = this.props
     return (
        <form onSubmit = { handleSubmit } >
          <div>
            <Field name = "address1" component={TextField} type="text"/>
          </div>
          <div>
            <Field name="address2" component={TextField} type="text"/>
          </div>
          <div>
            <Field name="postcode" component={TextField} type="text" />
          </div>
          <RaisedButton type="submit" label="Continue"/>
        </form>
     )
  }
}

AddressForm = reduxForm({
    form: 'contact',
    destroyOnUnmount: false,
    onSubmit: () => {
      let link = "etc"
      browserHistory.push(link)
    },
    validate
})(AddressForm)

export default AddressForm
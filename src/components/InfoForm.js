import RaisedButton from "material-ui/RaisedButton"

import React, { Component } from "react"

import { Field, reduxForm } from "redux-form"

import validate from './validate'
import renderField from './renderField'


class InfoForm extends Component {
  render() {
    const { handleSubmit } = this.props
     return (
        <form onSubmit = { handleSubmit } >
          <div>
            <label htmlFor="info1">Info 1 </label>
            <Field name = "info1" component={renderField} type="text"/>
          </div>
          <div>
            <label htmlFor="info2">Info 2 </label>
            <Field name="info2" component={renderField} type="text"/>
          </div>
          <div>
            <label htmlFor="info3">info 3 </label>
            <Field name="info3" component={renderField} type="text" />
          </div>
          <RaisedButton label="Complete" type="submit"/>
        </form>
     )
  }
}

InfoForm = reduxForm({
    form: 'contact',
    destroyOnUnmount: false, 
    validate
})(InfoForm)

export default InfoForm